﻿using System;

namespace ISPLibrary
{
    public class AudioBook : IBorrowableAudio
    {
        public string LibraryId { get; set; }
        public string Title { get; set; }
        public int RuntimeInMinutes { get; set; }
        public int CheckOutDurationDays { get; set; } = 14;
        public string Borrower { get; set; }
        public DateTime BorrowDate { get; set; }        
        public void CheckOut(string borrower)
        {
            Borrower = borrower;
            BorrowDate = DateTime.Now;
        }
        public void CheckIn()
        {
            Borrower = $"{Borrower} has returned item";
        }
        public DateTime GetDueDate()
        {
            return BorrowDate.AddDays(CheckOutDurationDays);
        }
    }
}
