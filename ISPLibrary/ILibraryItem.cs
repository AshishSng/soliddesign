﻿namespace ISPLibrary
{
    public interface ILibraryItem
    {
        string Title { get; set; }        
        string LibraryId { get; set; }
   }
}
