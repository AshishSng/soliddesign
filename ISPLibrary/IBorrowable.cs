﻿using System;

namespace ISPLibrary
{
    public interface IBorrowable
    {
        string Borrower { get; set; }
        DateTime BorrowDate { get; set; }        
        int CheckOutDurationDays { get; set; }
        void CheckIn();
        void CheckOut(string borrower);
        DateTime GetDueDate();
    }
}
