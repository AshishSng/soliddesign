﻿namespace ISPLibrary
{
    interface IBorrowableBook : IBook, IBorrowable
    {
    }
}
