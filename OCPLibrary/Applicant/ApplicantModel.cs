﻿namespace OCPLibrary
{
    public class ApplicantModel : IApplicantModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
