﻿namespace OCPLibrary
{
    public interface IExecutiveAccounts
    {
        IEmployeeModel Enroll(IApplicantModel person);
    }
}