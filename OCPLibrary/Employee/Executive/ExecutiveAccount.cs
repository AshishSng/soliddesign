﻿namespace OCPLibrary
{
    public class ExecutiveAccount : IExecutiveAccounts
    {
        IEmployeeModel _executive;
        public ExecutiveAccount(IEmployeeModel exectiveModel)
        {
            _executive = exectiveModel;
        }
        public IEmployeeModel Enroll(IApplicantModel person)
        {
            _executive.FirstName = person.FirstName;
            _executive.LastName = person.LastName;
            _executive.EmailAddress = $"{person.FirstName}.{person.LastName}@mycompanycrop.com";
            _executive.IsManager = true;
            _executive.IsExecutive = true;
            return _executive;
        }
    }
}
