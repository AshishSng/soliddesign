﻿namespace OCPLibrary
{
    public interface IStaffAccount
    {
        IEmployeeModel Enroll(IApplicantModel person);
    }
}