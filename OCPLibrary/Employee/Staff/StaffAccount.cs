﻿namespace OCPLibrary
{
    public class StaffAccount : IStaffAccount
    {
        IEmployeeModel _employee;               
        public StaffAccount (IEmployeeModel employeeModel)
        {
            _employee = employeeModel;
        }
        public IEmployeeModel Enroll (IApplicantModel person)
        {
            IEmployeeModel emp = new EmployeeModel();
            emp.FirstName = person.FirstName;
            emp.LastName = person.LastName;
            emp.EmailAddress = $"{person.FirstName.Substring(0,1)}.{person.LastName}@mycompany.com";

            return emp;
        }
    }
}
