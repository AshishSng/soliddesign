﻿namespace OCPLibrary
{
    public interface IEmployeeModel
    {
        string EmailAddress { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        bool IsManager { get; set; }
        bool IsExecutive { get; set; }
    }
}