﻿namespace OCPLibrary
{
    public interface IManagerAccounts
    {
        IEmployeeModel Enroll(IApplicantModel person);
    }
}