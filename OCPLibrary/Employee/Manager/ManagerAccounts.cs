﻿namespace OCPLibrary
{
    public class ManagerAccounts : IManagerAccounts
    {
        IEmployeeModel _manager;               
        public ManagerAccounts (IEmployeeModel managerModel)
        {
            _manager = managerModel;
        }
        public IEmployeeModel Enroll (IApplicantModel person)
        {            
            _manager.FirstName = person.FirstName;
            _manager.LastName = person.LastName;
            _manager.EmailAddress = $"{person.FirstName.Substring(0,1)}.{person.LastName}@mycompany.com";
            _manager.IsManager = true;
            return _manager;
        }
    }
}
