﻿using LSPLibrary;

namespace LSPConsoleUI
{
    public static class Factory
    {
        public static IManaged Mgr()
        {
            return new Manager();
        }
        public static IManager CmpyCEO()
        {
            return new CEO();
        }
        public static IManaged Emp()
        {
            return new Employee();
        }
    }
}
