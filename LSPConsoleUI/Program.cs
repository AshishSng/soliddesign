﻿using LSPLibrary;
using System;

namespace LSPConsoleUI
{
    public  class Program
    {
         static void Main(string[] args)
        {
            IManager accountingVP = Factory.CmpyCEO();

            accountingVP.FirstName = "Emma";
            accountingVP.LastName = "Stone";
            accountingVP.CalculatePerHourRate(4);

            Console.WriteLine($"{accountingVP.FirstName}'s salary is ${accountingVP.Salary}/hour.");

            IEmployee emp = Factory.Mgr();

            emp.FirstName = "Tim";
            emp.LastName = "Coray";
            //emp.AssignManager(accountingVP);
            emp.CalculatePerHourRate(2);

            Console.WriteLine($"{emp.FirstName}'s salary is ${emp.Salary}/hour.");

            Console.ReadLine();
        }
    }
}
