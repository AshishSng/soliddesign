﻿using OCPLibrary;
using System.Collections.Generic;

namespace OCPDataLayer
{
    public class ApplicantData : IApplicantData
    {
        List<IApplicantModel> _applicantList;
        IApplicantModel _applicant1;
        IApplicantModel _applicant2;
        IApplicantModel _applicant3;
        IApplicantModel _applicant4;
        IApplicantModel _applicant5;
        IApplicantModel _applicant6;

        public ApplicantData(List<IApplicantModel> applicantList, IApplicantModel applicant1, IApplicantModel applicant2,
            IApplicantModel applicant3, IApplicantModel applicant4, IApplicantModel applicant5, IApplicantModel applicant6)
        {
            _applicantList = applicantList;
            _applicant1 = applicant1;
            _applicant2 = applicant2;
            _applicant3 = applicant3;
            _applicant4 = applicant4;
            _applicant5 = applicant5;
            _applicant6 = applicant6;
        }

        public List<IApplicantModel> ListOfApplicants()
        {
            List<IApplicantModel> applicants = _applicantList;
            IApplicantModel applicant1 = _applicant1;
            IApplicantModel applicant2 = _applicant2;
            IApplicantModel applicant3 = _applicant3;
            IApplicantModel applicant4 = _applicant4;
            IApplicantModel applicant5 = _applicant5;
            IApplicantModel applicant6 = _applicant6;

            applicant1.FirstName = "Tim";
            applicant1.LastName = "Corey";
            applicants.Add(applicant1);

            applicant2.FirstName = "Sue";
            applicant2.LastName = "Storm";
            applicants.Add(applicant2);

            applicant3.FirstName = "Nancy";
            applicant3.LastName = "Roman";
            applicants.Add(applicant3);

            applicant4.FirstName = "Shane";
            applicant4.LastName = "Pilon";
            applicants.Add(applicant4);

            applicant5.FirstName = "Craig";
            applicant5.LastName = "Pitcher";
            applicants.Add(applicant5);

            applicant6.FirstName = "Kyle";
            applicant6.LastName = "Faila";
            applicants.Add(applicant6);

            return applicants;
        }
    }
}
