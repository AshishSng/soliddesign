﻿using OCPLibrary;
using System.Collections.Generic;

namespace OCPDataLayer
{
    public interface IApplicantData
    {
        List<IApplicantModel> ListOfApplicants();
    }
}