﻿using OCPDataLayer;
using OCPLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OCPConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IApplicantModel> applicantList = Factory.ApplicantList();           
            IApplicantData applicantApplied = Factory.ApplicantApplied();
            applicantList = applicantApplied.ListOfApplicants();

            List<IEmployeeModel> employee = Factory.EmployeeList();      

            employee.Add(Factory.StaffProcessor().Enroll(applicantList.ElementAt(0)));
            employee.Add(Factory.StaffProcessor().Enroll(applicantList.ElementAt(3)));

            employee.Add(Factory.ManagerProcessor().Enroll(applicantList.ElementAt(1)));
            employee.Add(Factory.ManagerProcessor().Enroll(applicantList.ElementAt(4)));

            employee.Add(Factory.ExecutiveProccessor().Enroll(applicantList.ElementAt(2)));
            employee.Add(Factory.ExecutiveProccessor().Enroll(applicantList.ElementAt(5)));

            foreach (var emp in employee)
            {
                Console.WriteLine($"{emp.FirstName} {emp.LastName} : {emp.EmailAddress} IsManager: {emp.IsManager} IsExecutive: {emp.IsExecutive}");
            }

            Console.ReadLine();
        }
    }
}
