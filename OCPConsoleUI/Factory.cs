﻿using OCPDataLayer;
using OCPLibrary;
using System.Collections.Generic;

namespace OCPConsoleUI
{
    public static class Factory
    {
        #region Applicant
        public static IApplicantModel Applicant()
        {
            return new ApplicantModel();
        }

        public static List<IApplicantModel> ApplicantList()
        {
            return new List<IApplicantModel>();
        }
        public static IApplicantData ApplicantApplied()
        {
            return new ApplicantData(ApplicantList(), Applicant(), Applicant(), Applicant(),
                Applicant(), Applicant(), Applicant());
        }
        #endregion

        #region Employee
        public static IEmployeeModel Employee()
        {
            return new EmployeeModel();
        }
        public static List<IEmployeeModel> EmployeeList()
        {
            return new List<IEmployeeModel>();
        }
        #endregion

        #region Staff
        public static IStaffAccount StaffProcessor()
        {
            return new StaffAccount(Employee());
        }
        #endregion

        #region Manager           
        public static IManagerAccounts ManagerProcessor()
        {
            return new ManagerAccounts(Employee());
        }
        #endregion

        #region Executive              
        public static IExecutiveAccounts ExecutiveProccessor()
        {
            return new ExecutiveAccount(Employee());
        }
        #endregion
    }
}
